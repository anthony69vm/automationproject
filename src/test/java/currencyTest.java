import dataproviders.currencyProvider;
import dataproviders.newCustomerProvider;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobject.CustomerPage;
import pageobject.homePage;
import pageobject.itemPage;
import pojo.currencyData;
import java.lang.Thread;




public class currencyTest extends baseTest {
    @Test(dataProvider = "getCurrencyFromJson",dataProviderClass = currencyProvider.class)
    public void currency_set(currencyData _currencyData) throws InterruptedException{
        homePage home = new homePage(baseURL,driver);
        itemPage item = new itemPage(baseURL,driver);
        home.clickItem();
        item.clickCurrency();
        item.clickEuroButton();
        //item.waitUntilElementExists(item.setTextEuro());
        Thread.sleep(6000);
        Assert.assertEquals(item.setTextCurrency().getText(), _currencyData.getEuro());
        item.clickCurrency();
        item.clickSterlingButton();
        Thread.sleep(6000);
        Assert.assertEquals(item.setTextCurrency().getText(), _currencyData.getPoundSterling());
        item.clickCurrency();
        item.clickDolarButton();
        Thread.sleep(6000);
        Assert.assertEquals(item.setTextCurrency().getText(), _currencyData.getUsd());




    }
}