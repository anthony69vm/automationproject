package dataproviders;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import org.testng.annotations.DataProvider;
import pojo.registerData;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class newCustomerProvider {
    @DataProvider(name="getCustomerDataFromJson")
    private Object[][] getCustomerDataFromJson() throws FileNotFoundException {
        JsonElement jsonData = new JsonParser().parse(new FileReader("src/test/Resources/data/customer.json"));
        JsonElement Dataset = jsonData.getAsJsonObject().get("Dataset");

        List<registerData> testData = new Gson().fromJson(Dataset, new TypeToken<List<registerData>>() {}.getType());

        Object[][] returnValue = new Object[testData.size()][1];

        int index = 0;

        for(Object [] each: returnValue) {
            each[0] = testData.get(index++);
        }

        return returnValue;
}}
