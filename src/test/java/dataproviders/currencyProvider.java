package dataproviders;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import org.testng.annotations.DataProvider;
import pojo.currencyData;
import pojo.registerData;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class currencyProvider {
    @DataProvider(name="getCurrencyFromJson")
    private Object[][] getCurrencyFromJson() throws FileNotFoundException {
        JsonElement jsonData = new JsonParser().parse(new FileReader("src/test/Resources/data/currency.Json"));
        JsonElement Dataset = jsonData.getAsJsonObject().get("currencyData");

        List<currencyData> testData = new Gson().fromJson(Dataset, new TypeToken<List<currencyData>>() {}.getType());

        Object[][] returnValue = new Object[testData.size()][1];

        int index = 0;

        for(Object [] each: returnValue) {
            each[0] = testData.get(index++);
        }

        return returnValue;
    }}