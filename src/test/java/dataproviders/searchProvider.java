package dataproviders;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import org.testng.annotations.DataProvider;
import pojo.registerData;
import pojo.searchData;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class searchProvider {
    @DataProvider(name="getSearchFromJson")
    private Object[][] getSearchFromJson() throws FileNotFoundException {
        JsonElement jsonData = new JsonParser().parse(new FileReader("src/test/Resources/data/search.json"));
        JsonElement Dataset = jsonData.getAsJsonObject().get("dataSearch");

        List<searchData> testData = new Gson().fromJson(Dataset, new TypeToken<List<searchData>>() {}.getType());

        Object[][] returnValue = new Object[testData.size()][1];

        int index = 0;

        for(Object [] each: returnValue) {
            each[0] = testData.get(index++);
        }

        return returnValue;
    }}