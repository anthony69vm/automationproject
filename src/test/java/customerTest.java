import dataproviders.newCustomerProvider;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobject.CustomerPage;
import pageobject.homePage;
import pageobject.successPage;
import pojo.registerData;


public class customerTest extends baseTest {
    @Test(dataProvider = "getCustomerDataFromJson",dataProviderClass = newCustomerProvider.class)
    public void customer_register(registerData _registerdata) throws InterruptedException{
        CustomerPage register = new CustomerPage(baseURL, driver);
        homePage home = new homePage(baseURL , driver);
        successPage success = new successPage(driver);
        home.clickMyAccount();
        home.clickRegister();
        register.insertName(_registerdata.getname());
        register.insertLastName(_registerdata.getLastname());
        register.insertEmail(home.getSaltString()+"@gmail.com");
        register.insertTelephone(_registerdata.getTelephone());
        register.insertPassword(_registerdata.getPassword());
        register.insertConfirm(_registerdata.getPassword());
        register.selectCheckBox();
        register.selectContinueButton();

        success.waitUntilElementExists(success.selectCongrats());
        Assert.assertEquals(success.selectCongrats().isDisplayed(),true);

    }

}
