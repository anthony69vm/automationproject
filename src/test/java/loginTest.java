
import io.qameta.allure.Owner;
import io.qameta.allure.Step;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import pageobject.loginPage;
import pageobject.dashboardPage;

import dataproviders.usersProvider;
import pojo.loginData;


public class loginTest extends baseTest {


    // @Test(priority = 1,invocationCount = 1)
    @Test(dataProvider = "getUserDataFromJson", dataProviderClass = usersProvider.class)
    public void allow_user_to_do_login(loginData _loginData)  throws InterruptedException  {
        loginPage login = new loginPage(baseURL, driver);
        dashboardPage dashboardPage = new dashboardPage(driver);
        login.insertEmail(_loginData.getEmail());
        login.insertPassword(_loginData.getPassword());
        login.clickLoginButton();

        login.waitUntilElementExists(dashboardPage.showDashboard());

        Assert.assertEquals(dashboardPage.showDashboard().isDisplayed(), true);
    }

    @Test(dataProvider = "getWrongUsersDataFromJson", dataProviderClass = usersProvider.class)
    public void do_not_allow_user_to_do_login(loginData _loginData)  throws InterruptedException  {
        loginPage login = new loginPage(baseURL, driver);
        login.insertEmail(_loginData.getEmail());
        login.insertPassword(_loginData.getPassword());
        login.clickLoginButton();

        // Explicit wait
        login.waitUntilElementExists(login.displayAlertMessage());

        Assert.assertEquals(login.displayAlertMessage().isDisplayed(), true);
    }

}
