import dataproviders.newCustomerProvider;
import org.testng.annotations.Test;
import pageobject.*;
import pojo.searchData;
import dataproviders.searchProvider;
import org.testng.Assert;
import org.testng.annotations.Test;


public class addCartTest extends baseTest {
        @Test(dataProvider = "getSearchFromJson",dataProviderClass = searchProvider.class)
        public void add_Cart(searchData _addCart) throws InterruptedException {
            homePage home = new homePage(baseURL, driver);
            searchPage search = new searchPage(baseURL, driver);
            itemPage item = new itemPage(baseURL, driver);
            checkOutCartPage checkout = new checkOutCartPage(baseURL,driver);


            home.insertSearch(_addCart.getSearch());
            home.setenter();
            search.clickItemMackBook();
            item.clickAddToCart();
            item.waitUntilElementExists(item.setSuccesAddToCart());
            Assert.assertEquals(item.setSuccesAddToCart().isDisplayed(), true);
            item.clickItembutton();
            item.clickCheckout();
           checkout.waitUntilElementExists(checkout.SetNotAvailable());
            Assert.assertEquals(checkout.SetNotAvailable().isDisplayed(),true);



        }
}
