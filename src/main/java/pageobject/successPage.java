package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class successPage extends BasePage {

    private By Congrats = By.id("common-success");
    public successPage (WebDriver driver){ super(driver);}
    public WebElement selectCongrats(){ return driver.findElement(Congrats);}
}
