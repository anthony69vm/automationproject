package pageobject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;

public class checkOutCartPage extends BasePage {
    public By notAvailable = By.xpath("//div[@class='alert alert-danger alert-dismissible']");


    public checkOutCartPage (String url , WebDriver driver){ super(url,driver);}
    public WebElement SetNotAvailable(){return driver.findElement(notAvailable);}

}
