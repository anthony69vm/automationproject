package pageobject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.WebDriver;

public class searchPage extends homePage {

    public searchPage (String url , WebDriver driver){ super(url,driver);}
    public By itemMackbook = By.xpath("//a[@href='https://demo.opencart.com/index.php?route=product/product&product_id=43&search=MacBook']");


    public WebElement setItemMackBook(){return driver.findElement(this.itemMackbook);}

    public void clickItemMackBook(){setItemMackBook().click();}

    }


