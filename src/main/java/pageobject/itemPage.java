package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class itemPage extends homePage {

    public itemPage (String url , WebDriver driver){ super(url,driver);}
    public By addToCart = By.id("button-cart");
    public By sucessAddToCart = By.xpath("//div[@class='alert alert-success alert-dismissible']");
    public By itemButton = By.xpath("//button[@class='btn btn-inverse btn-block btn-lg dropdown-toggle']");
    public By checkout = By.xpath("//a[@href='https://demo.opencart.com/index.php?route=checkout/checkout']");
    public By currency = By.xpath("//button[@class='btn btn-link dropdown-toggle']");
    public By euro = By.xpath("//button[@name='EUR']");
    public By sterling = By.xpath("//button[@name='GBP']");
    public By dolar = By.xpath("//button[@name='USD']");
    public By textCurrency = By.xpath("//ul[@class='list-unstyled']/li/h2");

    public WebElement setTextCurrency(){return driver.findElement(this.textCurrency);}
    public WebElement setAddToCart(){return driver.findElement(this.addToCart);}
    public WebElement setCurrencyButton(){return driver.findElement(this.currency);}
    public WebElement setEuroButton(){return driver.findElement(this.euro);}
    public WebElement setSterlingButton(){return driver.findElement(this.sterling);}
    public WebElement setDolarButton(){return driver.findElement(this.dolar);}
    public WebElement setSuccesAddToCart(){return driver.findElement(this.sucessAddToCart);}
    public WebElement setItemButton(){return driver.findElement(this.itemButton);}
    public WebElement setCheckOut(){return driver.findElement(this.checkout);}

    public void clickCurrency(){setCurrencyButton().click();}
    public void clickAddToCart(){setAddToCart().click();}
    public void clickItembutton(){setItemButton().click();}
    public void clickCheckout(){setCheckOut().click();}
    public void clickEuroButton(){setEuroButton().click();}
    public void clickSterlingButton(){setSterlingButton().click();}
    public void clickDolarButton(){setDolarButton().click();}



    }



