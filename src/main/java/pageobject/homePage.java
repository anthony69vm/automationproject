package pageobject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;


import org.openqa.selenium.WebDriver;

import java.security.Key;

public class homePage extends BasePage {
    private By myAccount = By.xpath("//a[@title='My Account']");
    private By register = By.xpath("//a[@href='https://demo.opencart.com/index.php?route=account/register']");
    private By search = By.xpath("//input[@class='form-control input-lg']");
    private By selectitem = By.xpath("//a[@href='https://demo.opencart.com/index.php?route=product/product&product_id=43']");




    public homePage (String url , WebDriver driver){ super(url,driver);}
    public WebElement setSelect(){return driver.findElement(this.selectitem);}
    public WebElement setSearch(){return driver.findElement(this.search);}
    public WebElement setMyAccount(){return driver.findElement(this.myAccount);}
    public WebElement setRegister(){return driver.findElement(this.register);}
    public void insertSearch(String search){setSearch().sendKeys(search);}
    public void clickItem(){setSelect().click();}
    public void clickMyAccount (){ setMyAccount().click(); }
    public void clickRegister(){ setRegister().click();}
    public void setenter(){setSearch().sendKeys(Keys.ENTER);}






}
