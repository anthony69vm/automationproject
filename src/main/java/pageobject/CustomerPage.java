package pageobject;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CustomerPage extends BasePage {
    private By nameTextBox = By.id("input-firstname");
    private By lastNameTextBox = By.id("input-lastname");
    private By emailTextBox = By.id("input-email");
    private By telephoneTextBox = By.id("input-telephone");
    private By passwordTextBox = By.id("input-password");
    private By confirmTextBox = By.id("input-confirm");
    private By agreeCheckBox = By.xpath("//input[@type='checkbox']");
    private By continueButton = By.xpath("//input[@value='Continue']");



    public CustomerPage (String url, WebDriver driver){ super(url, driver);}

    public WebElement setname(){return driver.findElement(this.nameTextBox);}
    public WebElement setlastname(){return driver.findElement(this.lastNameTextBox);}
    public WebElement setEmail(){return driver.findElement(this.emailTextBox);}
    public WebElement setTelephone(){return driver.findElement(this.telephoneTextBox);}
    public WebElement setPassword(){return driver.findElement(this.passwordTextBox);}
    public WebElement setConfirm(){return driver.findElement(this.confirmTextBox);}
    public WebElement setCheckbox(){return driver.findElement(this.agreeCheckBox);}
    public WebElement setContinueButton(){return driver.findElement(this.continueButton);}

    @Step
    public void insertName(String name){
        setname().sendKeys(name);
    }
    @Step
    public void insertLastName(String lastName){
        setlastname().sendKeys(lastName);
    }
    @Step
    public void insertEmail(String email){
        setEmail().sendKeys(email);
    }
    @Step
    public void insertTelephone (String telephone){
        setTelephone().sendKeys(telephone);
    }
    @Step
    public void insertPassword(String password){
        setPassword().sendKeys(password);
    }
    @Step
    public void insertConfirm(String password){
        setConfirm().sendKeys(password);
    }
    @Step
    public void selectCheckBox(){
        setCheckbox().click();
    }
    @Step
    public void selectContinueButton(){
        setContinueButton().click();

    }



    }

