package pojo;

public class currencyData {




    private String euro;
    private String poundSterling;
    private String usd;

    public currencyData(String _euro, String _poundSterling,String _usd) {
        this.euro = _euro;
        this.poundSterling = _poundSterling;
        this.usd = _usd;

    }

    public String getEuro() {
        return euro;
    }

    public String getPoundSterling() {
        return poundSterling;
    }
    public String getUsd() {
        return usd;
    }
}

