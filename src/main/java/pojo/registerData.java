package pojo;



public class registerData {
    private String name;
    private String lastname;
    private String telephone;
    private String email;
    private String password;

    public registerData(String _name,String _lastname,String _telephone,String _email, String _password) {
        this.name = _name;
        this.lastname = _lastname;
        this.email = _email;
        this.telephone = _telephone;
        this.password = _password;
    }
    public String getname(){ return name;}
    public String getLastname(){return lastname;}
    public String getEmail(){return email;}
    public String getTelephone(){return telephone;}
    public String getPassword(){return password;}

}
